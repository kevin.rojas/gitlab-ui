export const POPPER_CONFIG = {
  modifiers: [
    {
      name: 'offset',
      options: {
        offset: [0, 4],
      },
    },
  ],
};

// base dropdown events
export const GL_DROPDOWN_SHOWN = 'shown';
export const GL_DROPDOWN_HIDDEN = 'hidden';
export const GL_DROPDOWN_FOCUS_CONTENT = 'focusContent';

// KEY Codes
export const ARROW_DOWN = 'ArrowDown';
export const ARROW_UP = 'ArrowUp';
export const END = 'End';
export const ENTER = 'Enter';
export const HOME = 'Home';
export const SPACE = 'Space';
